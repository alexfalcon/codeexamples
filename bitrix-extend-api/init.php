<?
/* error_reporting(E_ALL); */
//
//Подключаем страницу с собственными константами
require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/php_interface/defines.php');
//Подключаем страницу ссобственными функциями	
 require_once($_SERVER["DOCUMENT_ROOT"].'/bitrix/php_interface/functions.php');
//Подключение ко втрой базе данных
/*$DB2name = 'bitrix_2';
	$DB2user = 'bitrix2';
	$DB2pass = 'bitrix2';
	$DB2host = '127.0.0.1';
	
	$DB2 = new CDatabase();
	if ($DB2->Connect($DB2host, $DB2name, $DB2user, $DB2pass)) {
	$connect2 = 'Connect';
	} */

CModule::AddAutoloadClasses(
        '', // не указываем имя модуля
        array(
           // ключ - имя класса, значение - путь относительно корня сайта к файлу с классом
		   // Здесь подключим необходимые классы. Впоследствии они будут инициализироваться в классе IndexClass.
				'IndexClass' => '/bitrix/php_interface/main/IndexClass.php',
               'ModulesData' => '/bitrix/php_interface/main/ModulesData.php',
			   'Debug' => '/bitrix/php_interface/main/Debug.php',
			   'Load' => '/bitrix/php_interface/main/Load.php'
       
        )
);

 
//Инициализация основного класса своего API
$API = &IndexClass::get_instance();
//Подключаем класс для работы с инфоблоками
CModule::IncludeModule("iblock");	
//Подключение контроллеров

//Контроллер шаблона

//Запускаем контроллер шаблона
$template_c = &$API->Load->controller('Template');
 $template_data = $template_c->index();
/*  if(isset($_POST['city_cookie'])) {
//Запускаем контроллер установки кук
$cookie_c = &$API->Load->controller('Cookie');
$cookie_data = $cookie_c->index();
} */
extract($template_data); 
