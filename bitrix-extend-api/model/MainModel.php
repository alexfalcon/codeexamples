<?
class MainModel {
	
	public function getList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array(), $moduleDataKey = false, $module = 'iblock') {
		//Проверяем, подключен ли модуль
		if (CModule::IncludeModule($module)) {
			$result_data = array();
			//Получаем объект с данными
			$data_obj = CIBlockElement::GetList($arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
			while($data_arr = $data_obj->GetNext()) {
				
				$result_data[] = $data_arr;
			}
			//Если установлен ключ для сохранения данных, чтобы повторно их использовать, то пишем выведенные данные в свойство класс ModulesData
			if($moduleDataKey) {
				$API = &IndexClass::get_instance();
				$API->ModulesData->set_data($moduleDataKey, $result_data);
			}
			return $result_data;
		}
		else return false;
	}
	
	public function sectGetList($arOrder = array(), $arFilter = array(), $IncCnt = false, $Select = array(), $navStartParams = false, $moduleDataKey = false, $module = 'iblock') {
		if (CModule::IncludeModule($module)) {
			$result_data = array();
			$arOrder['left_margin'] = 'asc';
			$Select[] = 'IBLOCK_SECTION_ID'; 
			//Получаем объект с данными
			$data_obj = CIBlockSection::GetList($arOrder, $arFilter, $IncCnt, $Select, $navStartParams);
			//Объявляем итератор. Он отображает ключ текущего родительского массива. Первый массив идет в корень резалта, поэтому после первой записи он долшжен принять значение 0
			$i = -1;
			while($data_arr = $data_obj->GetNext()) {
				
				//Если родительская категория корневая, то пишем $data_arr в корень результирующего массива
				
				 if($data_arr['IBLOCK_SECTION_ID'] == '') {
				 $data_arr['CHILDREN'] = array();
				$result_data[] = $data_arr;
				$i++;
				}
				///Иначе пишем $data_arr в элемент CHILDREN текущего родительского массива
				else {
					$result_data[$i]['CHILDREN'][] = $data_arr;
				} 
			}
			
			if($moduleDataKey) {
				$API = &IndexClass::get_instance();
				$API->ModulesData->set_data($moduleDataKey, $result_data);
			}
			
			return $result_data;
			
		}
		
	}
	
	public function getIblockProperty($iblockId, $elementId, $arOrder = array(), $arFilter = array()) {
		if (CModule::IncludeModule('iblock')) {
			$res = CIBlockElement::GetProperty($iblockId, $elementId, $arOrder, $arFilter);
			$result_data = array();
			while($data_arr = $res->GetNext()) {
				$result_data[] = $data_arr;
			}
			return $result_data;
		}
		else return false;
	}
	
	public function sectGetById($id) {
		$result_data = array();
		$res = CIBlockSection::GetByID($id);
		while($ar_res = $res->GetNext()) {
			$result_data[] = $ar_res;
		}
		return $result_data;
	}
}