<?

//Класс для хранения данных модулей для повторного использования
class ModulesData {
	
	private $data = array();
	
	public function set_data($key, $data) {
		$this->data[$key] = $data;
	}
	
	public function get_data($key) {
		if(isset($this->data[$key]))
		return $this->data[$key];
		else return false;
	}
}