<?

class IndexClass {
	static private $instance = null;
	public $ModulesData;
	public $Debug;
	public $Load;	
		
		
		//инициализация функция создает главный объект(если ранее не созавался), записывает в свойство и возвращает
		static function get_instance() {
			
			if(!self::$instance) {
				self::$instance = new IndexClass;
			}
			return self::$instance;
		
		}
		//
		function __construct () {
			$this->init();
		}
		
		private function init() {
			$this->ModulesData = new ModulesData;
			$this->Debug = new Debug;
			$this->Load = new Load;
		}
		
		
		public function test() {
			echo 'ok';
		}
		
}