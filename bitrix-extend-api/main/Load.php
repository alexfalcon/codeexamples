<?
class Load {
	private $controller = array();
	private $model = array();
/* 	private $application; */

	
	public function model($model) {
		if (!$this->$model[$model]) {
			require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/model/' . $model . '.php';
			$this->$model[$model] = new $model;
		}
		return $this->$model[$model];
	}
	
	public function controller($controller) {
		if (!$this->$controller[$controller]) {
			require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/controller/' . $controller . '.php';
			$this->$controller[$controller] = new $controller;
		}
		return $this->$controller[$controller];
	}
	
	
}