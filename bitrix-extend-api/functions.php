<?
//
//функция отладки выводит и содержание переменной
function print_var($var) {
	if(is_array($var)) {
	echo '<pre>';
	print_r($var);
	echo '</pre>';
	}
	if(is_object($var)){
		
		echo 'Класс: ' . get_class($var) . '<br />' .
		'Свойства: <br/>';
		foreach(get_class_vars($var) as $class_var => $val) {
			echo $class_var . ':' . $val . '<br />';
		}
		echo 'Методы: <br />';
		foreach(get_class_methods($var) as $class_method) {
			echo $class_method . '<br />';
		}
		
	}
	if(is_string($var)) {
		echo 'Тип: строка <br />Значение: ' . $var;
	}
}

//Функция инициализации собственного API (чтоб в клиентском коде не писать много букв)

function get_instance() {
	return IndexClass::get_instance();
}

function del_tag($tag, $text) {
	if(is_array($tag)) {
		foreach ($tag as $val) {
			$text = str_replace($val, '', $text);
		}
		return $text;
	}
	else return str_replace($tag, '', $text);
}

function get_preview($img) {
	$img_arr = explode('/', $img);
	$last_part = $img_arr[count($img_arr)-1];
	$dot_pos = strrpos($last_part, '.');
	$ext = substr($last_part, $dot_pos);
	$file = substr($last_part, 0, $dot_pos);
	 return 'http://pharmacosmetica.ru/cache/images/pharmacosmetica/'. $file . '_w200' . $ext;

	
}

function test1() {
	echo 11;
}