﻿
//Переменные для опр типа браузера
var isChrome = !!window.chrome && !!window.chrome.webstore;
var isFF = !!window.sidebar;


//window.onload-события главного шаблона 
window.onload = window.onerror = function() {
  if (!this.executed) { // выполнится только один раз
    this.executed = true;
	//Здесь сами события
	
	//Кроссбраузерность
	
	 if(isFF) {
	/* $('.product-item').css({'width' : '21.4em'}); */
	$('section.products .carou-wrap.module').css('height', '16.4em');
		
	}
	  mediaLoader('<', 791, [
	
	 {func: $prependElem, par: {target: $('#breadcrumb'), point: $('#main-content')} }
	
	]);
	
	mediaLoader('>', 791,[ 
	{func: $insertElem, par: {target: $('#breadcrumb'), point: $('#main-content aside'), action: 'after', ind: 444} }

	], true);  
	
  }
};
// отложенная загрузка this
window.onreadystatechange = function() {
  var self = this;
  if (this.readyState == "complete" || this.readyState == "loaded") {
    setTimeout(function() { self.onload() }, 0);
  }
};

/* ------ Конец Onload-событий --------- */



//В обертке
(function () {

//СТАТИЧЕСКИЕ ПЕРЕМЕННЫЕ MEDIA LOADER
//Индекс последнего слайда для карусели	(используется в вызове кастомизированного плагина carouFredSell)


/* mediaLoader.func = []; */

mediaLoader.func = false;

mediaLoader.params = [];



//Уже запущенные функции с параметрами
resizeHandler.runfunc = {};

mediaLoader.checkRun = {};
	
})();
//СТАТИЧЕСКИЕ ПЕРЕМЕННЫЕ ФУНКЦИЙ (в обертке)
(function () {
carouLastElem.val = 3;
$myToggle.v = false;
setBorders.ind = {};

setProductsBorders.ind = {};
})()
//Кроссбраузерный загрузчик скриптов относительно размеров окна (Аналог css Media Queries). При достижении опр значения ширины окна браузера выполняет пользовательскую функцию. В отличие от window.onresize, нужная функция запускается один раз на заданный диапазон значений ширины.

function mediaLoader(op, width, functions, notAuto) {
	var width = width - 15;
	var params = {};
	params.op = op;
	params.width = width;
	params.functions = functions;
	/* params.par = (par === void(0)) ? false : par;  */
	
	if(!notAuto) {
		check(params);
	}
	/* mediaLoader.func.push(check); */
	mediaLoader.func = check;
	mediaLoader.params.push(params);
	
	function check(params) {
		
		 if(params.op == '<') {
			
			if (document.documentElement.clientWidth < params.width) {
				
				fireFunc ();
				
			}
			else return false;
		}
		
		if(params.op == '>') {
			if (document.documentElement.clientWidth > params.width) {
				
				fireFunc ();
			}
			else return false;
		}

	 	if(params.op == '<=') {
			if (document.documentElement.clientWidth <= params.width) {	
				fireFunc();
			}
			else return false;
		}
		if(params.op == '>=') {
			if (document.documentElement.clientWidth >= params.width) {
				fireFunc();
			}
			else return false;
		}
		//Функция, запускающая переданную функцию и проводящая сравнение текущих и ранее переданных параметров при предыдущих ее вызовах
		 function fireFunc () {
			funcCicle:
			for(var i=0; i<params.functions.length; i++) {
				//Если паметров нет, то функции в качестве параметра передается false
				if(params.functions[i].par === void(0)) {
					params.functions[i].par = false;
				}
				
				
				//На данный момент переданные нам функции принимают в качестве параметров только объекты. впоследствие расширим.
				
				 if(typeof(params.functions[i].par) === 'object' && mediaLoader.checkRun[params.functions[i].func.name] != void(0)) {
					//Если функция запускалась ранее с такими же условиями, то переходим к сравнению аргументов
					if(compareSimArr(mediaLoader.checkRun[params.functions[i].func.name].options, [params.op, params.width])) {
				
					//Сравение аргументов. Если аргументы одинаковые, выходим из итерации главного цикла и переходим к след функции
					for(var i2=0; i2<mediaLoader.checkRun[params.functions[i].func.name].par.length; i2++) {
					
						if(compareObj(mediaLoader.checkRun[params.functions[i].func.name].par[i2], params.functions[i].par)) {			
						continue funcCicle;
						}

						/* compareSimArr(mediaLoader.checkRun[params.functions[i].func.name].options[i2], [params.op, params.width]); */
					}
					}
					//Если условия запуска функции уже другие, очищаем массивы условий и параметров
					else {
						mediaLoader.checkRun[params.functions[i].func.name].options.length = 0;
						mediaLoader.checkRun[params.functions[i].func.name].par.length = 0;
					}
				 }
				//Если прошли предыдущие проверки, то записываем параметры в объект checkRun. В каждом элементе-объекте, ключ которого имя запускаемой ниже функции, содержатся параметры вызова и условия вызова (разрешение экрана и оператор сравнения)
				
				if(mediaLoader.checkRun[params.functions[i].func.name] == void(0)) {
					mediaLoader.checkRun[params.functions[i].func.name] = {};
					mediaLoader.checkRun[params.functions[i].func.name].par = [];
					
				}
				
				mediaLoader.checkRun[params.functions[i].func.name].par.push(params.functions[i].par);
				
				mediaLoader.checkRun[params.functions[i].func.name].options =[params.op, params.width]; 
				
				params.functions[i].func(params.functions[i].par);
			}
		}  
			
	}
	
}
//Запускает функции из объекта mediaLoader.func по событию onresize
function resizeHandler () {
	
	window.onresize = function () {
		
		for(i=0; i<mediaLoader.params.length; i++) {
		
			mediaLoader.func(mediaLoader.params[i]);
			
		}
	}
}

//Функция сравнивает значения свойств идентичных объектов
function compareObj(obj1, obj2) {
	 if(typeof(obj1) != "object" || typeof(obj2) != "object") return false;
	var obj = (getKeysCount(obj1) >= getKeysCount(obj2)) ? obj1 : obj2;
	for(var key in obj) {
		if(typeof(obj1[key]) === "object" || typeof(obj2[key]) === "object" ) continue;
		if(obj1[key] != obj2[key]) return false;
	} 
	return true;
}

function getKeysCount(obj) {
  var counter = 0;
  for (var key in obj) {
    counter++;
  }
  return counter;
}
//Функция сравнения подобных массивов с одинак количеством элементов
 function compareSimArr(arr1, arr2) {
	if(typeof(arr1) != "object" || typeof(arr2) != "object") return false;
	
	 for(var i=0; i<arr1.length; i++) {
		if(arr1[i] != arr2[i]) return false;
	} 
		
	return true;
} 

//Конец кроссбраузерного загрузчика



//определяет последний элемент в слайдере
function carouLastElem(index) {
	carouLastElem.val = index.ind;
}
//Устатавливает границы у элементов каталога и слайдера
function setBorders (par) {
	 
		carouLastElem.val = par.ind;
		par.block.children().eq(par.ind).children('.product-item-inner').css('border', 'none');
		
		 par.block.children().eq(par.ind - 1).children('.product-item-inner').css('border-right', '0.1em dashed #bcbcbc');
		/* alert(carouLastElem.val);  */
		
	}
	
function setProductsBorders (par) {
	/* if(par.ind === setProductsBorders.ind[par.block.attr('id')]) return;
	else setProductsBorders.ind[par.block.attr('id')] = par.ind; */
	var i = par.ind;
	var i2 = par.ind-1;
	var itemsLength = par.block.children().length;
	while(i < itemsLength) {
	
		par.block.children().eq(i).children('.product-item-inner').css('border', 'none');
		  
		par.block.children().eq(i2).children('.product-item-inner').css('border-right', '0.1em dashed #bcbcbc');
		
		par.block.children().eq(i).children('.product-item-inner').children('.product-item-info').css('right', '-0.2em');
		
		i = i+par.ind+1;
		i2 = i2+par.ind;
	}  
	
}

//Функция переходя на др ресурс или страницу
function goTo (url) {
	document.location.href = url;
}

function changeBlocks (par) {
	var target = par.block;
	var point = par.point;
	var parent = target.parentNode;
	if(par.action === 'after') {
		insertAfter(target, point);
	}
	if(par.action === 'before') {
		parent.insertBefore(target, point);
	}
}

function insertAfter(elem, refElem) {
    var parent = refElem.parentNode;
    var next = refElem.nextSibling;
    if (next) {
        return parent.insertBefore(elem, next);
    } else {
        return parent.appendChild(elem);
    }
}

function append (par) {
	var $target = par.target;
	var $point = par.point;
	$point.append($target);
}

function $insertAfter (par) {
	var $target = par.target;
	var $point = par.point;
	$target.insertAfter($point);
}

function $insertElem(par) {
	var $target = par.target;
	var $point = par.point;
	if(par.action == 'after') {
	$target.insertAfter($point);
	}
	if(par.action == 'before') {
	$target.insertBefore($point);
	}
}

function $prependElem(par) {
	var $target = par.target;
	var $point = par.point;

	$target.prependTo($point);

}

function $appendElem(par) {
	var $target = par.target;
	var $point = par.point;

	$target.appendTo($point);

}



function $myToggle ($elem, func) {
	var i = 0;
	$elem.click(
	function() {
		
		if($myToggle.v === false) {
			func[i]($(this));
			$myToggle.v = true;
			if(i<func.length-1) {
			i++;
			}
			else i = 0;
			die();
		}
	
		if($myToggle.v === true) {
			func[i]($(this));
			$myToggle.v = false;
			if(i<func.length-1) {
			i++;
			}
			else i = 0;
		}
	}
	);
}

function setImgWidth() {
	var $textPage = $('#text-page');
	
	var $img = $('#text-page img');
	var textPageWidth = $textPage.innerWidth();
	var imgWidth = $img.innerWidth();
					
	/* if (textPageWidth >= imgWidth) {
		var relImgWidth = (imgWidth/$textPageWidth)*100;
	} */
					
	if(imgWidth < textPageWidth) {
		$img.width(imgWidth);
	}
	
	if(imgWidth >= textPageWidth) {
		$img.css('width', '100%');
	}
	
	$(window).resize(function() {
		if(imgWidth < $textPage.innerWidth()) {
			$img.width(imgWidth);
			/* alert(imgWidth);
			alert(textPageWidth); */
		}
		if(imgWidth >= $textPage.innerWidth()) {
			$img.css('width', '100%');
			/* alert(imgWidth);
			alert(textPageWidth); */
		}
	})
}

function setCenterPosition(elem) {
	 elem.style.top = Math.round(document.documentElement.clientHeight/2 - elem.clientHeight/2)+'px';
	elem.style.left = Math.round(document.documentElement.clientWidth/2 - elem.clientWidth/2)+'px';
}


