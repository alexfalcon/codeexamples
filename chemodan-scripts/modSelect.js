﻿ (function () {
	$( document ).ready(start);

	start.showList = false;
	start.listlock = false;
	start.curList = false;
	start.clickOut = false;
	start.curValues = {};
	start.imgItems = {};
	
	
	function start() {
		var $inputDiv = $('.input');
		var $msList = $('.input .msList');
		var $arrow = $('.input .arrow');
		$myToggleColors.v = {};
		
		$msList.css('width', $inputDiv.width()+3);
		$arrow.click(function(){
			$curList = $(this).next('.msList');
			$curList.fadeIn(100, function() {
				start.showList = true;
				start.curList = $curList;
				start.clickout = false;
			});
			
		});
		$msList.mouseover(function(){
			start.listlock = true;
		});
		$msList.mouseout(function(){
			start.listlock = false;
		});
		
		 
			 $( document ).click(function() {
				if(start.showList && !start.listlock && !start.clickOut) {
					
					$msList.fadeOut(100, function() {
					var textValuesArr = []; //?
					var textValues = '';
					var $msSelected = start.curList.parent().next('.msSelected');
					var $mainInput = start.curList.prevAll('input');
					var mainInputName = $mainInput.attr('name');
					start.curValues[mainInputName] = [];
					$msSelected.empty();				
					start.showList = false;
					var imgNum = 0;
					var $inputChecked = start.curList.find('input:checked');
					if($inputChecked.length > 0) {
					$inputChecked.parent().next().each(function(indx, element) {
						//В массив текстовые значения (пригодится при удалаении)
						textValuesArr.push($(element).html()); //?
						
						var textValue = $(element).html();
						var newSelectItem = document.createElement('div');
						textValues = textValues+textValue+', '
						newSelectItem.className = 'msSelectItem';
						newSelectItem.innerHTML = '<img class="del_'+imgNum+'" title="Удалить из списка" src="images/del-selected.png" /><div class="textSelected">'+textValue+'</div>';
						$msSelected.append(newSelectItem);
						imgNum++;
					})
					
					$mainInput.attr('value', textValues);
					start.curValues[mainInputName] = textValuesArr;
					/* alert(start.curValues[mainInputName]); */
					//
					//Отправляем кнопку удаления в функцию удаления
					delSelected($('.msSelectItem img'));
					}
					start.clickout = true;
					
				});
				}	
			});
			
			function delSelected ($delButton) {
				$delButton.click(function() {
					var mainInput = $(this).parent().parent().prev('.input').children('input');
					var mainInputName = mainInput.attr('name');
					var selectedName = $(this).next('.textSelected').html();
					var $checkbox = mainInput.nextAll('.msList').find('input[value="'+selectedName+'"]');
					//Получаем нужный индекс из картинки удаления
					var delElemIndex = +$(this).attr('class').split('_')[1];
					var curValuesArr = start.curValues[mainInputName];
					delete curValuesArr[delElemIndex];
					var newValuesArr = [];
					for(var i=0; i<curValuesArr.length; i++) {	
						if(typeof curValuesArr[i] != 'undefined') {
							newValuesArr.push(curValuesArr[i]);
						}		
					}
					var newValuesStr = newValuesArr.join(',');
					mainInput.attr('value', newValuesStr);
					if (mainInput.attr('value') == '') {
						mainInput.attr('value', 'Не важно');
					}
					//Удаляем нужный чекбокс
					$checkbox.removeAttr('checked');
					//Удаляем блок
					$(this).parent().remove();
					
				});
			}
			//Функция $myToggleColors
			 $myToggleColors($('.input-color .msList .color-wrap'),[selectColor, unSelectColor]);

			function selectColor(self) {
				self.css('border', '3px solid #a82922');
				/* var $mainInput = self.parent().prevAll('input'); */
				var $selectedColors = self.parent().prevAll('.colors');
				var $checkbox = self.children('input');
				var newColorDiv = document.createElement('div');
				var $color = self.children('.color');
				var newColorDivStyle = $color.css('background-color');
				if(!newColorDivStyle) {
					var newColorDivStyle = self.children('.color').css('background-image');
				}
				
				newColorDiv.className = 'selected-color '+$color.attr('id');
				newColorDiv.style.background = newColorDivStyle;
				if($selectedColors.children('span').length > 0){
					$selectedColors.empty();
				}
				$selectedColors.append(newColorDiv);
				$checkbox.attr('checked', 'checked');
				
				
			}
			function unSelectColor(self) {
				self.css('border', '3px solid #fff');
				var colorId = self.children('.color').attr('id');
				var $selectedColors = self.parent().prevAll('.colors');
				var $checkbox = self.children('input');
				$selectedColors.children('.selected-color.'+colorId).remove();
				$checkbox.removeAttr('checked');
				if($selectedColors.children().length < 1) {
					$selectedColors.html('<span>Не важно</span>');
				}
			}
			
			//Видоизмененная Togle - функция
			
			function $myToggleColors ($elem, func) {
				var i = {};
				
				$elem.click(
				function() {
				var colorDivId = $(this).children('.color').attr('id');
				if(typeof i[colorDivId] == 'undefined') {
					i[colorDivId] = 0;
				}
				if(typeof $myToggleColors.v[colorDivId] == 'undefined') {
					/* alert(colorDivId); */
					$myToggleColors.v[colorDivId] = false;
				}
				/* alert($myToggleColors.v[colorDivId]); */
				//Первое условие
				if($myToggleColors.v[colorDivId] === false) {
					func[i[colorDivId]]($(this));
					$myToggleColors.v[colorDivId]  = true;
					if(i[colorDivId]<func.length-1) {
					i[colorDivId]++;
				}
				else i[colorDivId] = 0;
				return;
				}
				//Второе условие
			if($myToggleColors.v[colorDivId] === true) {
				func[i[colorDivId]]($(this));
				$myToggleColors.v[colorDivId] = false;
				if(i[colorDivId]<func.length-1) {
				i[colorDivId]++;
				}
				else i[colorDivId] = 0;
			}
		}
		);
	}
	
	$('#clearForm').click(function(){
		
		$('#filter').find('input[type="text"]:not(.costFrom, .costTo)').attr('value', 'Не важно');
		$('#filter .costFrom').attr('value', 0);
		$('#filter .costTo').attr('value', 99999);
		$('#filter').find('input[type="checkbox"], input[type="radio"]').removeAttr('checked');
		$('#filter .msSelected .msSelectItem').remove();
		$('#filter .input.input-color .colors').html('<span>Не важно</span>');
		$('#filter .input.input-color .color-wrap').css('border', '3px solid #fff');
	});
			/* $('.input-color .msList .color-wrap').click(function(){
				$(this).css('border', '2px solid #a82922');
			}); */
			
			
		}
		  
	
 })();