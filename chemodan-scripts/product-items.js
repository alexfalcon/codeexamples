﻿//функционал списка товаров

(function () {
	//Статическая переменная для хранения текущего цвета 
	getProductImage.curColorItem = {};
	//Статическая переменная для хранения пути к текущей картинке
	getProductImage.curImageItem = {};
	/*  $(".product-item .product-item-colors .box").eq(getProductImage.curColorItem).addClass('box-active');  */
	$(".product-item .product-item-colors div.box:first-child").addClass('box-active');
	$(document).ready(setCompareLink);
	
	
		
	
})();
	//Первый параметр Id продукта без префикса, второй -ключ в массиве с цветами
	//Функцию надо будет отладить после подключения ajax
	function getProductImage(itemId, colorNum) {
		
	
		//Переключаем иконки цветов
		if(getProductImage.curColorItem[itemId] == void(0)) {
			var curColor = 0;
		}
		else {
			var curColor = getProductImage.curColorItem[itemId];
		}
		var box = $("#"+itemId+" .product-item-colors .box");
		 /* $("#"+itemId+" .product-item-colors .box").eq(curColor).removeClass('box-active');
		 $("#"+itemId+" .product-item-colors .box").eq(colorNum).addClass('box-active'); */
		 box.eq(curColor).removeClass('box-active');
		 box.eq(colorNum).addClass('box-active');
		 //Записываем текущий цвет в статический объект
		getProductImage.curColorItem[itemId] = colorNum;
		//Записываем путь к картинкам и ссылкам в статический объект (чтобы повторно не подгружать их с сервера)
		getProductImage.curImageItem[itemId][curColor]['image'] = $("#"+itemId+" .product-item-image a img").attr('src');
		getProductImage.curImageItem[itemId][curColor]['link'] = $("#"+itemId+" .product-item-image a").attr('href');
		//Если в нашем объекте нет картинки для запрошенного цвета, то подгружем ее по ajax с сервера
		if(getProductImage.curImageItem[itemId][colorNum]['image'] == void(0)) {
		//Выделяем id продукта в отдельную переменнную	
		var prodId = itemId.split('_')[1];
		 $.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'ajax/get_color_img.php',
			data: 'colorNum='+colorNum+'&prodId='+prodId,
			success: function(data){
				//в объекте data должны присутствовать элементы data.image(картинка), data.link(ссылка)
				//раскомментировать после настройки ajax
				/* setImage(itemId, data.image); */
			}

		});
		
		}
		else {
			//раскомментировать после настройки ajax
			/* setImage(itemId, getProductImage.curImageItem[itemId][colorNum]['image'], getProductImage.curImageItem[itemId][colorNum]['image']); */
		}		
	}
	//смена картинки
	//setImage(id контейнера, url картинки, ссылка на картинку)
	function setImage(itemId, imageUrl, link) {
		$("#"+itemId+" .product-item-image a").replaceWith('<a href="'+link+'"><img src="'+imageUrl+'" /></a>');
	}
	
	setCompareLink.data = {};
	function setCompareLink() {
		var checkbox = $('.product-item input');
		var checked = $('.product-item input:checked');
		var compareLink = checked.next();
		var compareText = 'Сравнить';
		var compareLinkText = '<span>Перейти к сравнению</span>';
		$setLink(compareLink, compareLinkText);
		checked.each(function(i){
				var prodId = this.name.split('_')[1];
				setCompareLink.data[prodId] = prodId;
		});
		
		checkbox.change(function(){
			if(this.checked) {
				setLink(this.parentNode.children[1], compareLinkText);
				var checkedCompareLink = this.parentNode.children[1];
				var prodId = this.name.split('_')[1];
				setCompareLink.data[prodId] = prodId;
				checkedCompareLink.onclick = sendItemsData;
				/* alertObj(setCompareLink.data); */
			}
			else {
				setLink(this.parentNode.children[1], compareText);
				var prodId = this.name.split('_')[1];
				delete setCompareLink.data[prodId];
				/* alertObj(setCompareLink.data); */
			}
		});
		
		  
		
		compareLink.click(sendItemsData);
		/* sendItemsData(); */
		
		function sendItemsData(){
			var vLock = document.createElement('div');
			vLock.id = 'vLock';
			vLock.style.height = document.body.clientHeight+'px';
			document.body.appendChild(vLock);
			
			$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'compare.php',
			data: setCompareLink.data,
			success: function(data){
				/* alert(data.result);  */
				createPopup(data.result);
			} 
			});
		}
		
		function createPopup(data) {
				var popup = document.createElement('div');
				popup.id = 'comparePopup';
				popup.innerHTML = data;
				document.body.appendChild(popup);
				setCenterPosition(popup);
				popupFunc();
		}
		
		function popupFunc () {
			var $comparePopup = $('#comparePopup');
			var $compareTable = $('#compare-table');
			var $lock = $('#vLock');
			
			if ($('#compare-table .head td').length > 4) {
				$comparePopup.css('overflow-x', 'scroll'); 
			}
			
			$('#back-to-catalog').click(closePopup);
			$('.cp-del img').click(function(){
				var delClass = $(this).parent().parent().attr('class');
				$('.'+delClass).remove();
			});
			
			$('#compare-table tr').each(function(i){
				this.children[this.children.length-1].style.borderRight = 'none';
			});
				
			function closePopup() {
				$comparePopup.remove();
				$lock.remove();
			}

			function delProduct() {
				
			}	
		
		}	
		
		
		function $setLink(e, text) {
			e.html(text);
			
		}
		
		function setLink(e, text) {
			e.innerHTML = text;
		}
		
	}
	
	function alertObj(obj) { 
    var str = ""; 
    for(k in obj) { 
        str += k+": "+ obj[k]+"\r\n"; 
    } 
    alert(str); 
}
	
	
	function compare() {
		
		
	}
	
	
	
	