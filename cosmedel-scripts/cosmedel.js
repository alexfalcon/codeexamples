
function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires*1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) { 
  	options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for(var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];    
    if (propValue !== true) { 
      updatedCookie += "=" + propValue;
     }
  }

  document.cookie = updatedCookie;
}
//Затемняет экран и центрирует переданный блок
function fixScreen (cBlock) {
	
	/* document.documentElement.style.position = 'fixed'; */
	var darkBlock = document.getElementById('dark-block');
	var cBlock = document.getElementById(cBlock);
	cBlock.style.display = 'block';
	var htmlHeight = document.body.scrollHeight ;
	 darkBlock.setAttribute('style', 'background: #000; opacity: 0.5; width: 100%; height: '+htmlHeight+'px; z-index: 50; display: block; filter: alpha(opacity=50);');
	var clientWidth = document.documentElement.clientWidth;
	var clientHeight = document.documentElement.clientHeight;
	cBlock.style.display = 'block';
	cBlock.style.position = 'fixed';
	cBlock.style.left = (Math.round(clientWidth / 2) - cBlock.clientWidth /  2) +'px';
	cBlock.style.top = (Math.round(clientHeight / 2) - cBlock.clientHeight /  2) +'px'; 
	
	
}

function closeFixScreen (cBlockId) {
	var cBlock = document.getElementById(cBlockId);
	var darkBlock = document.getElementById('dark-block');
	cBlock.style.display = 'none';
	darkBlock.removeAttribute('style');
}

/* Кабинет */
 function account_login(formId) {
		fixScreen(formId);
		/* var closeForm = document.getElementById('close_form');
		closeForm.setAttribute('onclick', 'returnToMain()'); */
	} 
//Войти в личный кабинет	
function to_account() {
		window.location = 'http://'+window.location.host+'/account/';
	}
//Вернуться на главную	
function returnToMain () {
	window.location = 'http://'+window.location.host;
}
//Скрывает элемент
function hideElement (elemId) {
	var elem = document.getElementById(elemId);
	elem.style.display = 'none';
}
//Устанавливает селект
function setSelected(prefix, id) {
	
	if(id) {
	var elemId = prefix+'_'+id;
	var option = document.getElementById(elemId);
	option.setAttribute('selected', 'selected');
	}
	else return false;
}

//Устанавливает checked
function setChecked(prefix, id) {
	if(id) {
	var elemId = prefix+'_'+id;
	var input = document.getElementById(elemId);
	input.setAttribute('checked', 'checked');
	}
	else return false;
}
