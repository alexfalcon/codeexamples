<?
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
$GLOBALS['APPLICATION']->RestartBuffer();

$API = &get_instance();
$MainModel = &$API->Load->model('MainModel');

if(!isset($_POST['gamma'])){
 
$products_data = $MainModel->getList(Array(), array('IBLOCK_ID' => 8 ,  "PROPERTY_PR_BRAND" => $_POST['brand_id'], "!PROPERTY_GAMMA" => false), false, false, array('ID', 'PROPERTY_GAMMA', 'PROPERTY_TEXTURA2', 'PROPERTY_DESTINATION', 'PROPERTY_PROBLEM', 'PROPERTY_TYPE_OF_COSMETICS', 'PROPERTY_TYPE_SKIN', 'PROPERTY_USAGE_AREA'));
$gammas_val = array();
$textura2_val = array();
$destination_val = array();
$problem_val = array();
$type_of_cosmetics_val = array();
$type_of_skin_val = array();
$usage_area_val = array();
$get_filter = array();
//Записываемп в хранилище
$API->ModulesData->set_data('smart_filter_products_data', $products_data);
foreach($products_data as $product) {
	/* $detail_picture = CFile::GetPath($gamma["DETAIL_PICTURE"]); */
	$gammas_val[] = $product['PROPERTY_GAMMA_VALUE'];
	$textura2_val[] = $product['PROPERTY_TEXTURA2_VALUE'];
	$destination_val[] = $product['PROPERTY_DESTINATION_VALUE'];
	$problem_val[] = $product['PROPERTY_PROBLEM_VALUE'];
	$type_of_cosmetics_val[] = $product['PROPERTY_TYPE_OF_COSMETICS_VALUE'];
	$type_of_skin_val[] = $product['PROPERTY_TYPE_SKIN_VALUE'];
	$usage_area_val[] = $product['PROPERTY_USAGE_AREA_VALUE'];
	
}
$gammas_val = array_unique($gammas_val);
sort($gammas_val);
$textura2_val = array_unique($textura2_val);
$destination_val = array_unique($destination_val);
$problem_val = array_unique($problem_val);
$type_of_cosmetics_val = array_unique($type_of_cosmetics_val);
$type_of_skin_val = array_unique($type_of_skin_val);
$usage_area_val = array_unique($usage_area_val);

foreach ($gammas_val as $key => $val) {
	if($val == '') unset($gammas_val[$key]);
}
foreach ($textura2_val as $key => $val) {
	if($val == '') unset($textura2_val[$key]);
}
foreach ($destination_val as $key => $val) {
	if($val == '') unset($destination_val[$key]);
}
foreach ($problem_val as $key => $val) {
	if($val == '') { 
	unset($problem_val[$key]);
	}
	/* else {
		if(strpos($val, ', ')) {
			explode (', ')
		}
	} */
} 
foreach ($type_of_cosmetics_val as $key => $val) {
	if($val == '') unset($type_of_cosmetics_val[$key]);
} 
foreach ($type_of_skin_val as $key => $val) {
	if($val == '') unset($type_of_skin_val[$key]);
}   
foreach ($usage_area_val as $key => $val) {
	if($val == '') unset($usage_area_val[$key]);
}     

//Бренды 

$brands = $API->ModulesData->get_data('brands');
foreach ($brands as $brand) {
	if($brand['XML_ID'] == $_POST['brand_id']) {
		$detail_picture = $brand['PROPERTY_IMAGE_VALUE'];
	}
}
//Данные фильтра $_GET
$get_filter = array();
$result_array = array('gammas' => $gammas_val, 'detail_picture' => $detail_picture, 'textura2' => $textura2_val, 'destination' => $destination_val, 'problem' => $problem_val, 'type_of_cosmetics' => $type_of_cosmetics_val, 'type_of_skin' => $type_of_skin_val, 'usage_area' => $usage_area_val, 'get_filter' => $get_filter, 'products_data' => $products_data );

 echo json_encode ($result_array); 
/* print_var($result_array['usage_area']); */

}
