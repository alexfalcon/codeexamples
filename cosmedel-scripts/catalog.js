	(function() {
	
	var curBrand = document.getElementById('brand_smart_35');
	curBrand.setAttribute('selected', 'selected');
	var smartSelectBrand = document.getElementById('smart-filter-select');
	var smartSelectGamma = document.getElementById('smart-filter-select-gamma');
	
	getBrandInfo(smartSelectBrand.value);
	
	smartSelectBrand.onchange = function () {
		/* alert(smartSelectBrand.value); */
		getBrandInfo(smartSelectBrand.value);
		
		}
		
	smartSelectGamma.onchange = function () {
			 
			getGammaInfo(smartSelectGamma.value);
		}
	}) ();
//Функция получения брендов	
	function getBrandInfo (smartSelectBrand) {
		 $.ajax({
  type: 'POST',
  dataType: 'json',
  url: '/bitrix/templates/cosmedel/components/bitrix/catalog.smart.filter/.default/ajax_brand.php',
  data: 'brand_id='+smartSelectBrand,
  success: function(data){
getBrandInfo.productsData = data['products_data'];
 var smartSelectGamma = document.getElementById('smart-filter-select-gamma');
 var option;
	//Удаляем текущие значения
	var options = smartSelectGamma.getElementsByTagName('option');
	 /* alert(options.length);  */
	var oLength = options.length;
	if(options.length > 1) {
		 for(var i=0; i<oLength-1; i++) {
			/* alert(i); */
			 smartSelectGamma.removeChild(options[1]);	
		} 
		
	}
	for (var key in data['gammas']) {
		option = document.createElement('option');
		option.id = 'smart_gamma_'+data['gammas'][key];
		if(data['gammas'][key] == '') option.selected = 'selected';
		option.value =  data['gammas'][key];
		option.innerHTML = data['gammas'][key];
		smartSelectGamma.appendChild(option);
	}
	//Image
	var pictureDiv = document.getElementById('smart-filter-image');
	var prevImg = pictureDiv.childNodes;
	if(prevImg[0]) pictureDiv.removeChild(prevImg[0]);
	var picture = document.createElement('img');
	picture.width = 180;
	picture.src = data['detail_picture'];
	pictureDiv.appendChild(picture);
	
	var mainContainerUl = document.getElementById('smart-properties');
	/* var prevMainValues = mainContainerUl.getElementsByTagName('li');
	var prevMainValuesLength = prevMainValues.length;
	
	if(prevMainValuesLength > 0) {
	//Очищаем контейнер со свойствами
	for(var i=0; i<prevMainValuesLength; i++) {
		mainContainerUl.removeChild(prevMainValues[0]);
	}
	}  */
 	/* var test = [];
	 for(var key in data['destination']) {
	 alert(key+' = '+data['destination'][key]);
	}  */ 


	mainContainerUl.innerHTML = '';
	
	
	
	setCheckboxes( data['problem'], mainContainerUl, 'problem', 'Проблема', true);
	setCheckboxes( data['destination'], mainContainerUl, 'destination', 'Назначение', true);
	setCheckboxes( data['type_of_skin'], mainContainerUl, 'type_skin', 'Тип кожи', true);
	setCheckboxes( data['textura2'], mainContainerUl, 'textura2', 'Тип продукта, текстура');
	setCheckboxes( data['usage_area'], mainContainerUl, 'usage_area', 'Зона применения', true);
	setCheckboxes( data['type_of_cosmetics'], mainContainerUl, 'type_of_cosmetics', 'Вид косметики');
		getGammaInfo(smartSelectGamma.value); 
  }
});
}

getBrandInfo.productsData = false;

function getGammaInfo (smartSelectGamma) {
		
	  /* if(smartSelectGamma == '') {
	 
		
	 } */ 
	var gammas_val = [];
	var textura2_val = [];
	var destination_val = [];
	var problem_val = [];
	var type_of_cosmetics_val = [];
	var type_skin_val = [];
	var usage_area_val = [];
	var iAr = 0;
	if(smartSelectGamma) {
	for(i=0; i<getBrandInfo.productsData.length; i++) {
		if(getBrandInfo.productsData[i]['PROPERTY_GAMMA_VALUE'] == smartSelectGamma ) {
		
			textura2_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_TEXTURA2_VALUE'];
			destination_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_DESTINATION_VALUE'];
			problem_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_PROBLEM_VALUE'];
			type_of_cosmetics_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_TYPE_OF_COSMETICS_VALUE'];
			type_skin_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_TYPE_SKIN_VALUE'];
			usage_area_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_USAGE_AREA_VALUE'];
			iAr++;
		}
	}
	}
	else {
	
	for(i=0; i<getBrandInfo.productsData.length; i++) {
			textura2_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_TEXTURA2_VALUE'];
			destination_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_DESTINATION_VALUE'];
			problem_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_PROBLEM_VALUE'];
			type_of_cosmetics_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_TYPE_OF_COSMETICS_VALUE'];
			type_skin_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_TYPE_SKIN_VALUE'];
			usage_area_val[iAr] = getBrandInfo.productsData[i]['PROPERTY_USAGE_AREA_VALUE'];
			iAr++;	
	}
	
	}
	/* var xx = problem_val;
	var aa = separateElemArr(problem_val); */
	
	textura2_val = uniqueArr(separateElemArr(textura2_val));
	
	destination_val = uniqueArr(separateElemArr(destination_val));
	 
	problem_val = uniqueArr(separateElemArr(problem_val));
	
	type_of_cosmetics_val = uniqueArr(separateElemArr(type_of_cosmetics_val));
	
	type_skin_val = uniqueArr(separateElemArr(type_skin_val));
	
	usage_area_val = uniqueArr(separateElemArr(usage_area_val)); 
	
	//Главный контейнер для вставки свойств
	var mainContainerUl = document.getElementById('smart-properties');
	mainContainerUl.innerHTML = '';
	/* alert(1); */
	setCheckboxes2( problem_val, mainContainerUl, 'problem', 'Проблема');
	setCheckboxes2( destination_val, mainContainerUl, 'destination', 'Назначение');
	setCheckboxes2( type_skin_val, mainContainerUl, 'type_skin', 'Тип кожи');
	setCheckboxes2( textura2_val, mainContainerUl, 'textura2', 'Тип продукта, текстура');
	setCheckboxes2( usage_area_val, mainContainerUl, 'usage_area', 'Зона применения');
	setCheckboxes2( type_of_cosmetics_val, mainContainerUl, 'type_of_cosmetics', 'Вид косметики');
	
		
}

function setCheckboxes (data, mainContainer, prop_code, prop_name, multiple) {
	//Выбираем главный контейнер куда будем записывать данные
	//Если нет свойств, то выходим
	if(isEmpty(data)) return false; 
	//Если имеются строки с множественными значениями, то сделаем их каждым элементом основного массива значений
	if(multiple) {
		var elemArr = [];
		for(var key in data) {
			 if(~data[key].indexOf(', ')) {
				elemArr = data[key].split(', ');
				for (var i=0; i<elemArr.length; i++) {
					data[ucFirst(elemArr[i])] = ucFirst(elemArr[i]);
				}
				delete data[key]; 
			}	
		}
	data = uniqueObj(data);
	}
	var valContainerId = 'ul_'+prop_code;
	var li_lv1 = document.createElement('li');
	li_lv1.setAttribute('class', 'lv1');
	 li_lv1.innerHTML = '<a href="#" onclick="BX.toggle(BX(\'ul_'+prop_code+'\')); return false;" class="showchild">'+prop_name+'</a>\
				<ul id="'+valContainerId+'">\
				</ul>'; 
	mainContainer.appendChild(li_lv1);
	var container = document.getElementById(valContainerId);
	var li_lvl2;
	container.innerHTML = '';
	/* var prevValues = container.getElementsByTagName('li');
	var prevValuesLength = prevValues.length;
	if(prevValuesLength > 0) {
	//Очищаем контейнер со свойствами
	for(var i=0; i<prevValuesLength; i++) {
		container.removeChild(prevValues[0]);
	}
	} */
	
	
	for(var key in data) {
		
		/* if(data[key] == '') selected = 'checked="checked"'; */
		/* else selected = ''; */
		li_lvl2 = document.createElement('li');
		li_lvl2.setAttribute('class', 'lvl2');
		li_lvl2.innerHTML = '<table> \
		<tr> \
		<td valign="top"> \
		 <input id="'+prop_code+'_'+data[key]+'" type="checkbox" name="arrFilter_pf['+prop_code+'][]" value="'+data[key]+'" />\
		</td> \
		<td valign="top"> \
		<label for="'+prop_code+'_'+data[key]+'">'+data[key]+'</label> \
		</td> \
		</tr> \
		</table> \
		';
		container.appendChild(li_lvl2);
	}
	var tarElem;	
}


function setCheckboxes2 (data, mainContainer, prop_code, prop_name) {
	
	if(data.length == 0) return false;
	
	var valContainerId = 'ul_'+prop_code;
	var li_lv1 = document.createElement('li');
	li_lv1.setAttribute('class', 'lv1');
	 li_lv1.innerHTML = '<a href="#" onclick="BX.toggle(BX(\'ul_'+prop_code+'\')); return false;" class="showchild">'+prop_name+'</a>\
				<ul id="'+valContainerId+'">\
				</ul>'; 
	mainContainer.appendChild(li_lv1);
	var container = document.getElementById(valContainerId);
	var li_lvl2;
	container.innerHTML = '';
	for(i=0; i<data.length; i++) {
		li_lvl2 = document.createElement('li');
		li_lvl2.setAttribute('class', 'lvl2');
		li_lvl2.innerHTML = '<table> \
		<tr> \
		<td valign="top"> \
		 <input id="'+prop_code+'_'+data[i]+'" type="checkbox" name="arrFilter_pf['+prop_code+'][]" value="'+data[i]+'" />\
		</td> \
		<td valign="top"> \
		<label for="'+prop_code+'_'+data[i]+'">'+data[i]+'</label> \
		</td> \
		</tr> \
		</table> \
		';
		container.appendChild(li_lvl2);
	}
}

function isEmpty(obj) {
  for (var key in obj) {
    return false; 
  }

  return true;
}

//Конвертер объекта
function oc(a) {
var o = {};
for(var i=0;i<a.length;i++) {
o[a[i]]='';
}
return o;
}

function ucFirst(str) {
  var newStr = str.charAt(0).toUpperCase();

  for(var i=1; i<str.length; i++) {
    newStr += str.charAt(i);
  }

  return newStr;
}

function uniqueObj(obj) {
	var resultObj = {}
	for(var key in obj) {
		if(obj[key] != '') resultObj[obj[key]] = obj[key];
	}
	return resultObj;
}

function uniqueArr (arr) {
	if (arr.length == 0) return arr;
	var o = [];
	for(i=0; i<arr.length; i++) {
		if(o[arr[i]] == void(0)) {
		o[arr[i]] = true;
		}
		 else {
		arr.splice(i, 1);
		--i;
		} 
	}
	return arr;
}

function separateElemArr (arr) {
	var elemArr = [];
	var sepArr =[];
		for(i=0; i<arr.length; i++) {
			if((arr[i] == void(0)) || (arr[i] == '')) {
				arr.splice(i, 1);
				--i;
				continue;
			}
			 if(~arr[i].indexOf(', ')) {
				elemArr = arr[i].split(', ');
				for (var i2=0; i2<elemArr.length; i2++) {
					 sepArr.push(ucFirst(elemArr[i2]));
				}
				arr.splice(i, 1);
				--i;		
			}	
		}
		if(sepArr.length == 0) return arr;
	return (arr.length == 0) ? [] : arr.concat(sepArr);
}

//Сброс фильтров
document.getElementById('del_filter').onclick = function () {
	$('.lv1 ul li input').removeAttr('checked');
	 document.getElementById('smart_disabled_gamma').selected = 'selected';
	 document.getElementById('smart_disabled_brand').selected = 'selected';
}