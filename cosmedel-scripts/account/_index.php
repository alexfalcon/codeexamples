<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Личный кабинет");
$APPLICATION->SetTitle("Title");
?> <?
global $USER;
if($USER->IsAuthorized()) {
$user_groups = $USER->GetUserGroupArray();
// Если юзер принадлежит группе Комюзеры, то подгружаем личный кабинет
if(in_array(6, $user_groups)) {
if (isset($_GET['login'])){
	  header("Location: " .SITE_URL . $APPLICATION->GetCurPage());
	
}
else {	
$API = &get_instance();
$Account = &$API->Load->model('AccountModel');
$user = $Account->uGetByID($USER->GetId());
/* print_var($user); */
?> 	<?if($_GET['newuser'] && !isset($_COOKIE['registration'])) {
	setcookie('registration', 1, time() + 60 * 60 * 24 * 30 * 12);
	?> 		
<div id="success_auth"> Поздравляем вас с успешной регистрацией!</div>
 		
<script>
			setTimeout(
				function() {
					hideElement('success_auth');
				}, 3000
			);
		</script>
 	<?
	}
	?> 
<div id="account_header"><span class="head_cab">Личный кабинет</span><span class="username">&bull; <?=$user['NAME']?> <?=$user['SECOND_NAME']?> <?=$user['LAST_NAME']?></span> </div>
 
<table cellspacing="0" class="acc_menu"> 		
  <tbody>
    <tr> 			<td onclick="link_lk('LK')" class="active" id="LK_b">Ваш кабинет</td> 			<td onclick="link_lk('lk_change_pd')" id="lk_change_pd_b">Изменить персональные данные</td> 			<td onclick="link_lk('lk_change_password')" id="lk_change_password_b">Изменить пароль</td> 			<td> 			<form id="lk_logout"> <input type="hidden" value="yes" name="logout" /> 			<span onclick="document.getElementById('lk_logout').submit()"> Выйти</span> </form> 			</td> 		</tr>
   	</tbody>
</table>
 	
<div id="user_block" class="user"> 	
<!------------1st--------->
 	
  <div style="display: block;" id="LK" class="lk_block"> 		
    <div id="LK_feedback"> 		
<!--<form id="LK_feedback">
			<h3>Форма обратной связи</h3>
			<i>Если у Вас возникли вопросы, воспользуйтесь формой обратной связи и мы обязательно ответим Вам.</i>
			<input type="text" placeholder="Тема сообщения"/>
			<textarea placeholder="Текст сообщения"></textarea>
			<input type="submit" value=" "/>
		</form>-->
<?$APPLICATION->IncludeComponent(
	"cosmedel:main.feedback_lk",
	"",
	Array(
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => "Сообщение успешно отправлено",
		"EMAIL_TO" => "",
		"REQUIRED_FIELDS" => array('SUBJECT','MESSAGE'),
		"EVENT_MESSAGE_ID" => array(),
		"EVENT_NAME" => "FEEDBACK_FORM"
	)
);?> 	</div>
   		
    <div class="personal_data"> 			
      <h3>Персональные данные</h3>
     			 Фамилия: <b><?=$user['LAST_NAME']?></b>
      <br />
     			 Имя: <b><?=$user['NAME']?></b>
      <br />
     			 Отчество: <b><?=$user['SECOND_NAME']?></b>
      <br />
     			 Ваш email: <b><?=$user['EMAIL']?></b>
      <br />
     		</div>
   		
    <div style="clear: both;">
      <br />
    
      <br />
    
      <br />
    </div>
   		
    <div class="pd_raffle"> 			
      <div class="cloudlet">
        <h3>Конкурсы</h3>
      </div>
     			 			
<!--Вывод как у анимаций,только без картинок-->
 			
<div class="news-list"> 
				<div class="news-item" id="">
					<div class="news-date">05.01.2013</div>
					<div class="news-title">Конкурс от Vichy</div>
					А вы знаете, что кожа лица очень часто испытывает дефицит кислорода? Стресс, плохая экология, неправильное питание - все эти факторы ухудшают процесс естественного дыхания кожи. Но теперь появилась новинка - серия кислородных масок Beauty Style, которая разработана на базе уникальных методик и в прямом смысле слова дарит коже возможность дышать!
					<br/><br/>
					<a id="bxid_825344" class="news-detail-link" href="" >Подробнее...</a>
					<div style="clear:both"></div>
				</div>
			</div> 
 		</div>
   		
    <div class="pd_liveliness"> 			
      <div class="cloudlet">
        <h3>Анимации</h3>
      </div>
     			
      <div class="news-list"> 
<!--Вывод как у анимаций,только без картинок-->
 				
        <div id="" class="news-item"> 					
          <div class="news-date">05.01.2013</div>
         					
          <div class="news-title">День Красоты с &ldquo;Avene&rdquo;</div>
         					Место проведения: 					
          <ul class="anim-adress"> 						
            <li>м.Речной Вокзал, Ул. Фестивальная д. 63, корп. 1 </li>
           						
            <li>м. Речной Вокзал, Ул. Дыбенко д.26 корп.3 </li>
           						
            <li>м. Проспект Вернадского, ул. Лобачевского д.20</li>
           					</ul>
         					
          <br />
         					<a class="news-detail-link" >Подробнее...</a> 					
          <div style="clear: both;"></div>
         				</div>
       			</div>
     		</div>
   		
    <div style="clear: both;"></div>
   	</div>
 		
<!------------2nd--------->
 		<form id="lk_change_pd" class="lk_block"> 		
    <table> 			
      <tbody>
        <tr> 				<td> 					Фамилия*
            <br />
           					<input type="text" name="" /> 				</td> 				<td> 					Имя* 
            <br />
           					<input type="text" name="" /> 				</td> 				<td> 					Отчество*
            <br />
           					<input type="text" name="" /> 				</td> 			</tr>
      
        <tr> 				<td>					 					E-mail*
            <br />
           					<input type="text" name="" /> 				</td> 				<td> 					Дата рождения
            <br />
           						<select class="dd" name=""> 							<option> </option> 						</select> 						<select class="month" name=""> 							<option> </option> 						</select> 						<select class="YYYY" name=""> 							<option> </option> 						</select> 				</td> 				<td> 					Телефон
            <br />
           					<input type="text" name="" /> 				</td> 			</tr>
      
        <tr> 				<td>					 					Регион
            <br />
           					<select name=""> 						<option> </option> 					</select> 				</td> 				<td> 					Любимая марка косметики
            <br />
           					<select name=""> 						<option> </option> 					</select> 				</td> 				<td> 					Пол
            <br />
           					
            <div class="radio-block"> 						<input type="radio" id="man" name="" /><label for="man">мужской</label> 						<input type="radio" id="woman" name="" /><label for="woman">женский</label> 					</div>
           				</td> 			</tr>
      
        <tr> 				<td colspan="3"> 					<input type="submit" value=" " /> 				</td> 			</tr>
       		</tbody>
    </table>
   	</form> 	<form id="lk_change_password" class="lk_block">
<!------------3--------->
 		
    <table> 			
      <tbody>
        <tr> 				<td> 					Старый пароль
            <br />
           					<input type="password" name="" /> 				</td> 				 			</tr>
      
        <tr> 				<td> 					Новый пароль
            <br />
           					<input type="password" name="" /> 				</td> 				 			</tr>
      
        <tr> 				<td>					 					Подтвердите новый пароль
            <br />
           					<input type="password" name="" /> 				</td> 				 			</tr>
      
        <tr> 				<td> 					<input type="submit" value=" " /> 				</td> 			</tr>
       		</tbody>
    </table>
   	</form> 	</div>
 <?
}
} //--------- Конец личного кабинета ------
	else {?> 		
<h1><a href="<?=SITE_URL . '/bitrix/admin/'?>" >Войти в админ-панель</a></h1>
 		 	<?}
	
}
else {?> 	
<h1>Вход в личный кабинет</h1>
 	 	
<p onclick="account_login('login_form')" class="account-link">Войти в личный кабинет</p>
 	
<p><a class="account-link" href="/account/registration.php" >Регистрация</a></p>
 	
<p><a class="account-link" href="/account/forgot.php?forgot_password=yes" >Напомнить пароль</a></p>
 	 	<?$APPLICATION->IncludeComponent(
	"undefined",
	"",
Array(),
false
);?> 		
<script>
		account_login('login_form');
		</script>
 	<?}?> <?
?> 	
<script>
		function link_lk (block_id) {
			var userBlock = document.getElementById('userBlock');
			var item = document.getElementById(block_id);
			var prev = link_lk.cur;
			var menuPrev = document.getElementById(link_lk.cur.id+'_b');
			var menuItem = document.getElementById(item.id+'_b');
			menuPrev.removeAttribute('class');
			prev.style.display = 'none';
			menuItem.setAttribute('class', 'active');
			item.style.display = 'block';
			link_lk.cur = item;
			
		}
		//Начальная стр по умолчанию
		link_lk.cur = document.getElementById('LK');
	</script>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>