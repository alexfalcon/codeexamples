<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Восстановление пароля");
$APPLICATION->SetTitle("Восстановление пароля");
?>
<h1>Восстановление пароля</h1>

<?
global $USER;
if(!$USER->IsAuthorized()) {
	
	$APPLICATION->IncludeComponent(
		"bitrix:system.auth.forgotpasswd",
		"",
		false);
	
}
else {?>
	<p>Вы уже авторизованы</p>
<?}
	
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>