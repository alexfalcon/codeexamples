<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Личный кабинет");
$APPLICATION->SetTitle("Title");
?> <?
global $USER;
if($USER->IsAuthorized()) {
$user_groups = $USER->GetUserGroupArray();
// Если юзер принадлежит группе Комюзеры
 if(in_array(6, $user_groups) || in_array(1, $user_groups)) {
 //Если входим после логина то перезагружаем еще раз страницу, чтобы не просили ввести данные повторно
 if (isset($_GET['login'])){
	  header("Location: " .SITE_URL . $APPLICATION->GetCurPage());
	
}

$API = &get_instance();
$Account = &$API->Load->model('AccountModel');
$user = $Account->uGetByID($USER->GetId());
/* print_var($user); */
?>
	<?
	//Если после регистрации, то поздравлялка 
	if($_GET['newuser'] && !isset($_COOKIE['registration'])) {
	setcookie('registration', 1, time() + 60 * 60 * 24 * 30 * 12);
	?>
		<div id="success_auth"> Поздравляем вас с успешной регистрацией!</div>
		<script>
			setTimeout(
				function() {
					hideElement('success_auth');
				}, 3000
			);
		</script>
	<?
	}
	?>
<div id="account_header"><span class="head_cab">Личный кабинет</span><span class="username">&bull; <?=$user['NAME']?> <?=$user['SECOND_NAME']?> <?=$user['LAST_NAME']?></span> </div>

<table cellspacing="0" class="acc_menu"> 		
  <tbody>
    <tr> 			<td onclick="link_lk('LK')" id="LK_b">Ваш кабинет</td> 			<td onclick="link_lk('lk_change_pd')" id="lk_change_pd_b">Изменить персональные данные</td> 			<td onclick="link_lk('lk_change_password')" id="lk_change_password_b">Изменить пароль</td><td><form id="lk_logout"> <input type="hidden" value="yes" name="logout" /> 			<span onclick="document.getElementById('lk_logout').submit()"> Выйти</span> </form> 			</td> </tr>
   	</tbody>
</table>
<script>
	
	var cur_sect = false;
</script>
<?

?> 
<div id="user_block" class="user"> 
<!-- 0 страница детального просмотра конкурса -->
<?if($_GET['comp']) {
	$APPLICATION->IncludeComponent("bitrix:news.detail", "competitions", array(
	"IBLOCK_TYPE" => "catalog",
	"IBLOCK_ID" => "14",
	"ELEMENT_ID" => $_GET["comp"],
	"ELEMENT_CODE" => "",
	"CHECK_DATES" => "N",
	"FIELD_CODE" => array("ACTIVE_FROM", "DETAIL_TEXT", "NAME"),
	"PROPERTY_CODE" => array(),
	"IBLOCK_URL" => "",
	"AJAX_MODE" => "N",
	"AJAX_OPTION_JUMP" => "N",
	"AJAX_OPTION_STYLE" => "Y",
	"AJAX_OPTION_HISTORY" => "N",
	"CACHE_TYPE" => "N",
	/* "CACHE_TIME" => "36000000",
	"CACHE_GROUPS" => "Y", */
	"META_KEYWORDS" => "-",
	"META_DESCRIPTION" => "-",
	"BROWSER_TITLE" => "-",
	"SET_TITLE" => "Y",
	"SET_STATUS_404" => "N",
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
	"ADD_SECTIONS_CHAIN" => "Y",
	"ACTIVE_DATE_FORMAT" => "d.m.Y",
	"USE_PERMISSIONS" => "N",
	"DISPLAY_TOP_PAGER" => "N",
	"DISPLAY_BOTTOM_PAGER" => "N",
	"PAGER_TITLE" => "Страница",
	"PAGER_TEMPLATE" => "",
	"PAGER_SHOW_ALL" => "N",
	"DISPLAY_DATE" => "Y",
	"DISPLAY_NAME" => "Y",
	"DISPLAY_PICTURE" => "Y",
	"DISPLAY_PREVIEW_TEXT" => "Y",
	"AJAX_OPTION_ADDITIONAL" => ""
	),
	false
);
?>
	<script>
		//Начальная стр по умолчанию
		
		cur_sect = document.getElementById('raffle');
	</script>

<?
}?>	
<!------------1st--------->
 	
  <div id="LK" class="lk_block"> 		
    <div id="LK_feedback"> 		
<!--<form id="LK_feedback">
			<h3>Форма обратной связи</h3>
			<i>Если у Вас возникли вопросы, воспользуйтесь формой обратной связи и мы обязательно ответим Вам.</i>
			<input type="text" placeholder="Тема сообщения"/>
			<textarea placeholder="Текст сообщения"></textarea>
			<input type="submit" value=" "/>
		</form>-->
<?$APPLICATION->IncludeComponent(
	"cosmedel:main.feedback_lk",
	"",
	Array(
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => "Сообщение успешно отправлено",
		"EMAIL_TO" => "",
		"REQUIRED_FIELDS" => array('SUBJECT','MESSAGE'),
		"EVENT_MESSAGE_ID" => array(68),
		"EVENT_NAME" => "FEEDBACK_FORM"
	)
);?> 	</div>
   		
    <div class="personal_data"> 			
      <h3>Персональные данные</h3>
     			 Фамилия: <b><?=$user['LAST_NAME']?></b>
      <br />
     			 Имя: <b><?=$user['NAME']?></b>
      <br />
     			 Отчество: <b><?=$user['SECOND_NAME']?></b>
      <br />
     			 Ваш email: <b><?=$user['EMAIL']?></b>
      <br />
     		</div>
   		
    <div style="clear: both;"><br/><br/><br/></div>
   		
    <div class="pd_raffle">
      <div class="cloudlet">
        <h3>Конкурсы</h3>
      </div>
     			 			
<!--Вывод как у анимаций,только без картинок-->
	
  <?
?>
<? $APPLICATION->IncludeComponent("bitrix:news.list", "competitions", array(
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => "14",
				"NEWS_COUNT" => "100",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "ID",
				"SORT_ORDER2" => "DESC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"PROPERTY_CODE" => array(),
				"CHECK_DATES" => "N",
				"DETAIL_URL" => "/account/?comp=#ELEMENT_ID#",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "N",
				/* "CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N", */
				 "CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Конкурсы",
				"PARENT_SECTION" => "",
				"INCLUDE_SUBSECTIONS" => "Y",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "Y",
				"AJAX_OPTION_ADDITIONAL" => "",
				
				),
				false
			); ?>
	
<!--<div class="news-list"> 
				<div class="news-item" id="">
					<div class="news-date">05.01.2013</div>
					<div class="news-title">Конкурс от Vichy</div>
					А вы знаете, что кожа лица очень часто испытывает дефицит кислорода? Стресс, плохая экология, неправильное питание - все эти факторы ухудшают процесс естественного дыхания кожи. Но теперь появилась новинка - серия кислородных масок Beauty Style, которая разработана на базе уникальных методик и в прямом смысле слова дарит коже возможность дышать!
					<br/><br/>
					<a id="bxid_825344" class="news-detail-link" href="" >Подробнее...</a>
					<div style="clear:both"></div>
				</div>
			</div> -->
 		</div>
   		
    <div class="pd_liveliness"> 			
      <div class="cloudlet">
        <h3>Анимации</h3>
      </div>
     <?$APPLICATION->IncludeComponent("bitrix:news.list", "liveliness_lk", array(
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => "12",
				"NEWS_COUNT" => "100",
				"SORT_BY1" => "ACTIVE_FROM",
				"SORT_ORDER1" => "DESC",
				"SORT_BY2" => "ID",
				"SORT_ORDER2" => "DESC",
				"FILTER_NAME" => "",
				"FIELD_CODE" => array('PREVIEW_TEXT', 'NAME', 'ID'),
				"PROPERTY_CODE" => array(
					0 => "adress",
					1 => "phone",
					2 => "site",
					3 => "work_time",
					4 => "map"
				),
				"CHECK_DATES" => "N",
				"DETAIL_URL" => "",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"AJAX_OPTION_HISTORY" => "N",
				"CACHE_TYPE" => "N",
				/* "CACHE_TIME" => "36000000",
				"CACHE_FILTER" => "N", */
				"CACHE_GROUPS" => "Y",
				"PREVIEW_TRUNCATE_LEN" => "",
				"ACTIVE_DATE_FORMAT" => "d.m.Y",
				"SET_TITLE" => "N",
				"SET_STATUS_404" => "N",
				"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
				"ADD_SECTIONS_CHAIN" => "N",
				"HIDE_LINK_WHEN_NO_DETAIL" => "N",
				"PARENT_SECTION" => "",
				"PARENT_SECTION_CODE" => "",
				"INCLUDE_SUBSECTIONS" => "N",
				"DISPLAY_TOP_PAGER" => "N",
				"DISPLAY_BOTTOM_PAGER" => "N",
				"PAGER_TITLE" => "Анимации",
				"PAGER_SHOW_ALWAYS" => "N",
				"PAGER_TEMPLATE" => "",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"DISPLAY_DATE" => "N",
				"DISPLAY_NAME" => "Y",
				"DISPLAY_PICTURE" => "N",
				"DISPLAY_PREVIEW_TEXT" => "N",
				"AJAX_OPTION_ADDITIONAL" => ""
				),
				false
			);?>			
      <!--<div class="news-list"> 

 				
        <div id="" class="news-item"> 					
          <div class="news-date">05.01.2013</div>
         					
          <div class="news-title">День Красоты с &ldquo;Avene&rdquo;</div>
         					Место проведения: 					
          <ul class="anim-adress"> 						
            <li>м.Речной Вокзал, Ул. Фестивальная д. 63, корп. 1 </li>				
            <li>м. Речной Вокзал, Ул. Дыбенко д.26 корп.3 </li>			
            <li>м. Проспект Вернадского, ул. Лобачевского д.20</li>
           					</ul>
         					
          <br />
  <a class="news-detail-link" >Подробнее...</a> 					
          <div style="clear: both;"></div>
         				</div>
       			</div> -->
     		</div>
   		
    <div style="clear: both;"></div>
   	</div>
	<!--2-->
	
	 <?$APPLICATION->IncludeComponent(
	"bitrix:main.profile",
	"lk_change",
	Array(
		"USER_PROPERTY_NAME" => "",
		"SET_TITLE" => "Y",
		"AJAX_MODE" => "N",
		"USER_PROPERTY" => array("UF_CITY", "UF_F_BRAND"),
		"SEND_INFO" => "N",
		"CHECK_RIGHTS" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N"
	)
);?>
	 
		<?$APPLICATION->IncludeComponent(
		"bitrix:system.auth.forgotpasswd",
		"lk",
		false);?>
	
	</div>
	<script>
			<?if($_GET['change_lk'] == 'Y') {?>
				var firstBlock = 'lk_change_pd';
			<?}
			elseif($_GET['forgot_password'] == 'yes') {?>
				var firstBlock = 'lk_change_password';
			<?}
			else {?>				
				var firstBlock = 'LK';	
			<?}?>
			
			if(cur_sect == false) {
				cur_sect = document.getElementById(firstBlock);
			}
		cur_sect.style.display = 'block';
		var cur_menu = document.getElementById(cur_sect.id+'_b');
		if(cur_menu != undefined) {
			cur_menu.setAttribute('class', 'active');
		}
		function link_lk (block_id) {
			/*  alert (link_lk.cur.id); */
			var userBlock = document.getElementById('userBlock');
			var item = document.getElementById(block_id);
			var prev = link_lk.cur;
			var menuPrev = document.getElementById(link_lk.cur.id+'_b');
			var menuItem = document.getElementById(item.id+'_b');
			if(menuPrev != undefined) {
				menuPrev.removeAttribute('class');
			}
			
			prev.style.display = 'none';
			
			menuItem.setAttribute('class', 'active');
			item.style.display = 'block';
			link_lk.cur = item;	
		}
		link_lk.cur = cur_sect;
		/*  alert(link_lk.cur); */
		
	</script>
<?
}
	else {?>
		<h1><a href="<?=SITE_URL . '/bitrix/admin/'?>">Войти в админ-панель</a></h1>
		
	<?}
	
}
else {?>
	<h1>Вход в личный кабинет</h1>
	
	<p class="account-link" onclick="account_login('login_form')">Войти в личный кабинет</p>
	<p><a class="account-link" href="/account/registration.php">Регистрация</a></p>
	<p><a class="account-link" href="/account/forgot.php?forgot_password=yes">Напомнить пароль</a></p>
	
	<?
	if(!$_GET['logout_butt']) {?>
		<script>
		account_login('login_form');
		</script>
	<?}?>
<?}
?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>