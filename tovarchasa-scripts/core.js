﻿
var tChStartForm = true;

function tChForm(par) {
	//Запуск кроссбраузерной функции Onload
	myOnload (window, function () {
		/* Если переменной jQuery после загрузки страницы не обнаружено, то подгружаем библиотеку */
		
		 if(typeof jQuery === 'undefined') { 
			var jqScript = document.createElement('script');
			jqScript.src = 'http://code.jquery.com/jquery-latest.min.js';
			document.head.appendChild(jqScript);
			//только после подгрузки библиотеки стартуем плагин
			myOnload(jqScript, function() {
						startPlugin();
					}
			);
		}
		//Если установлена библиотека, то стартуем плагин
		else startPlugin();
	}	
	);	

	//ФУНКЦИЯ СТАРТА
	function startPlugin() {
	
		//Основные параметры
		var host = 'http://tovarchasa.ru/api/';
		var formStyles = host+'formCss.css';
		
		//Объект создания формы
		var createForm = {
			formUrl: host+'phpform.php',
			userId: par.userId,
			containerId: 'tChContainer',
			tChContainer: false,
			sendHandler: host+'send.php'
		};
			
			createForm.index= function() {
				/* if(typeof par.charset != 'undefined') {
					var charset = par.charset;
				}
				else {
					var charset = 'utf-8';
				} */
				////Подключаем cookie.js
				var jqCookie = document.createElement('script');
				jqCookie.src = host+'cookie.js';
				document.head.appendChild(jqCookie);
				//Запрашиваем форму по аякс
					var aData = toolsBox.ajax({
						async: false,
						type: "GET",
						url: this.formUrl,
						callBack: this.setForm,
						context: this,
						data: 'id='+par.userId
					});
					
				};
				//Колбэк-функция инсталяции формы
			createForm.setForm = function(result) {
				/* alert(result.project_data.ref); */
				var self = this;
				/* var linkCss = document.createElement('link');
				linkCss.rel = 'stylesheet';
				linkCss.href = formStyles;
				document.head.appendChild(linkCss);  */
				 /* myOnload (linkCss, function () {   */
					
					var tChContainer = document.createElement('div');
					tChContainer.id = self.containerId;
					tChContainer.innerHTML = result.form;
				
					document.body.appendChild(tChContainer);
					self.tChContainer = tChContainer;
					var containerForm = document.getElementById('tChForm');
					
					/* if(typeof par.inviteText != 'undefined') {
						containerForm.children[1].innerHTML = par.inviteText;
					} */
					
					//установка стилей по умолчанию 
					
					setStyles(result.project_data.default_styles);
					
					//Параметры из кода на странице
					if(typeof par.styles != 'undefined') {
						setStyles(par.styles);
					}
					
					//Установка свойств из данных проекта
				
					if(typeof result.project_data.styles != 'undefined') {
						setStyles(result.project_data.styles);
					}
					
					//Установка и возврат параметров главного контейнера
					var containerCond = setContainerPosition(tChContainer, par.formPosition);
					var $container = $('#'+self.containerId);
					
					
					toolsBox.MyToggle($('#tChContainer #tChlabel'),[formSlideIn, formSlideOut]);
					//
					var animation = {};
					if(par.formPosition == 'left') {	
						animation.inp = {left : 0};
						animation.out = {left : (0 - containerCond.outPropertyValue)+'px'};
					}
					if(par.formPosition == 'right') {	
						animation.inp = {right : 0};
						animation.out = {right : (0 - containerCond.outPropertyValue)+'px'};
					}
					
					if(typeof par.autoslide == 'undefined') {
					//Устанавливаем куки
					if(!$.cookie('formSlided')) {
						formSlideIn();
						$.cookie('formSlided', 'yes');
						toolsBox.MyToggle($('#tChContainer #tChlabel'),[formSlideOut, formSlideIn]);
					}
					else {
						toolsBox.MyToggle($('#tChContainer #tChlabel'),[formSlideIn, formSlideOut]);
					}
					
					}
					
					function formSlideIn() {
							
								 $container.animate(animation.inp, 500); 
							 
							 $('#tChContainer #tChlabel span.tCharrow').html('←');
					};
					function formSlideOut() {
							
								 $container.animate(animation.out, 500); 
							 
							 $('#tChContainer #tChlabel span.tCharrow').html('→');
					};
					
					
					//Запуск функции отсылки
						self.sendForm();
					/*   }, 10000);  */
					
				
				function setStyles(obj) {
					for(var key in obj ) {
						$(key).css(obj[key]);
					}
				}
				
				function setContainerPosition(container, position) {
					var prop = {};
					if(position == 'left') {
					 prop.positionProperty = 'top';
					 prop.positionParametr = 'clientHeight';
					 prop.outProperty = 'left';
					 prop.outParametr = 'clientWidth';
					 prop.containerCss = {
						'border-radius': '0 0 10px 0',
						'-webkit-border-radius': '0 0 10px 0',
						'-moz-border-radius': '0 0 10px 0'
					 };
					 prop.labelCss = {
						'right' : '-30px',
						'border-radius': '0 10px 10px 10px',
						'-webkit-border-radius': '0 10px 10px 0',
						'-moz-border-radius': '0 10px 10px 0'
					 };
					 
					}
					if(position == 'right') {
					 prop.positionProperty = 'top';
					 prop.positionParametr = 'clientHeight';
					 prop.outProperty = 'right';
					 prop.outParametr = 'clientWidth';
					 prop.containerCss = {
						'border-radius': '0 0 0 10px',
						'-webkit-border-radius': '0 0 0 10px',
						'-moz-border-radius': '0 0 0 10px'
					 };
					 prop.labelCss = {
						'left' : '-30px',
						'border-radius': '10px 0px 0px 10px',
						'-webkit-border-radius': '10px 0px 0px 10px',
						'-moz-border-radius': '10px 0px 0px 10px'
					 };
					}
					if(position == 'bottom') {
					 prop.positionProperty = 'left';
					 prop.positionParametr = 'clientWidth';
					 prop.outProperty = 'bottom';
					 prop.outParametr = 'clientHeight';
					 prop.labelCss = {
						'top' : '-30px',
						'border-radius': '0 10px 10px 10px',
						'-webkit-border-radius': '0 10px 10px 0',
						'-moz-border-radius': '0 10px 10px 0'
					  };
					}
					var contValue = container[prop.positionParametr];
					var clientValue = document.documentElement[prop.positionParametr];
					//Если моб устройства
					if ((navigator.userAgent.indexOf ('Android') != -1) || (navigator.userAgent.indexOf ('iOS') != -1)) {
						if(position == 'left' || position == 'right') {
							var value = Math.round(clientValue/10);
						}
						else {
							var value = Math.round(clientValue/2 - contValue/2);
						}
					}else {
					var value = Math.round(clientValue/2 - contValue/2);
					}
					container.style[prop.positionProperty] = value+'px';
					var out = container[prop.outParametr]+2;
					container.style[prop.outProperty] = (0 - out)+'px';
					prop.positionPropertyValue = value;
					prop.outPropertyValue = out;
					
					//css
					 $('#tChContainer #tChlabel').css(prop.labelCss);
					 $('#tChContainer').css(prop.containerCss);
					return prop;
					
				}
				
				
				
			};
			
			createForm.sendForm = function() {
				var self = this;
				var submit = document.getElementById('tChSubmit');
				
					var name = document.getElementsByName('tChName')[0];
					var phone = document.getElementsByName('tChPhone')[0];
					/* var email = document.getElementsByName('tChEmail')[0]; */
					var email = '';
					var	info = document.getElementsByName('tChInfo')[0];
					var hidden = document.getElementsByName('tChHForm')[0];
					
					 phone.onkeypress = function(ev) {
						phone.onkeyup = function() {
		
						if(phone.value.match(/^(.*[\+].*[\+]){1,}|(.*[\(].*[\(]){1,}|(.*[\)].*[\)]){1,}|(.*[\s].*[\s]){1,}|[^\d\s\+\(\)\-]$/)) {
						phone.value = phone.value.substr(0, phone.value.length-1);
						}
			
					} 
					} 
				
				submit.onclick = function() {	
					var noError = true;
					if(name.value.length == 0) {
						name.style.border = '2px solid red';
						submitError('empty');
						noError = false;
					}
					  if(phone.value.length == 0) {
						phone.style.border = '2px solid red';
						/* email.style.border = '2px solid red'; */
						submitError('empty');
						noError = false;
					}
					
					   
					
					if(name.value.length > 0 && !name.value.match(/^[A-Za-zА-Яа-я\s-]+$/)) {
						name.style.border = '2px solid red';
						submitError('errName');
						noError = false;
					}

					  /* if(phone.value.length > 0 && !phone.value.match(/^[0-9-\(\)\s]+$/)) {
						phone.style.border = '2px solid red';
						submitError('errPhone');
						noError = false;
					} */ 
						/* alert(par.userId); */
					if(typeof par.charset == 'undefined') {
						var charset = 'utf-8';
					}
					else {
						var charset = par.charset;
					}
					if(noError) {
					  toolsBox.ajax({
						async: true,
						type: "GET",
						url: self.sendHandler,
						data: 'name='+name.value+'&phone='+phone.value+'&email=&info='+info.value+'&id='+par.userId+'&hidd='+hidden.value+'&charset='+charset,
						callBack: sendSuccess,
						context: self
					}); 
					
					}
					
				}
				
				
				
				// перехват клавиш
				function catchKey (ev) {
	
					var key = ev.keyCode;
					if(!key) return false;
					return key;
	
				}
				submitError.flag = false;
				function submitError(error) {
					
					var tChError = document.getElementById('tChError');
					if(error == 'empty') {
						var msg = 'Заполните обязательные поля!';
					}
					if(error == 'errPhone') {
						var msg = 'Некорректно указан номер телефона';
					}
					if(error == 'errName') {
						var msg = 'Имя должно состоять только из русских или латинских букв';
					}
					
					if(error == 'contact') {
						var msg = 'Оставьте в одном из полей свои контактные данные';
					}
					
					if(error == 'numAbs') {
						var msg = 'В номере телефона недостаточно символов';
					}
					
					tChError.innerHTML = msg;
					tChError.style.color = 'red';
					tChError.style.marginBottom = '10px';
					submitError.flag = true;
				}
				
				function sendSuccess(msg) {
					var success = document.getElementById('tChSuccess');
					
					/* success.innerHTML = msg; */
					success.innerHTML = 'Ваша заявка отправлена. Наш менеджер свяжется с Вами в ближайшее время';
					name.value = '';
					phone.value = '';
					info.value = '';
					email.value = '';
					$("input[style*='red']").css('border', '1px solid #ccc');
					$('#tChError').empty();
					/* alert(success); */
				}
				
				
			};
			
			

		//Сундучок с инструментами
		var toolsBox = {
			toggleCond: {}
		};
		
			function resH(par) {
alert(par);
}
			
			toolsBox.ajax = function(ajaxPar) {
					/* alert(jQuery); */
					/* $('h1').css('color', 'red'); */
					 $.ajax({
					async: ajaxPar.async,
					type: ajaxPar.type,
					dataType : 'jsonp',
					jsonp: 'resH',
					url: ajaxPar.url,
					data: ajaxPar.data,
					success: function(msg){
						/* var res = msg */

						/* ajaxPar.callBack(msg); */
						 ajaxPar.callBack.call(ajaxPar.context, msg);
						
					}
					});
					 
			};
			//Toggle - функция
			toolsBox.MyToggle = function ($elem, func) {
				var i = 0;
				var elemId = $elem.attr('id');
				
				if(typeof this.toggleCond[elemId] === 'undefined') {
					this.toggleCond[elemId] = false;
				}
				$elem.click(
				function() {

			if(toolsBox.toggleCond[elemId] === false) {
				func[i]();
				toolsBox.toggleCond[elemId] = true;
				if(i<func.length-1) {
				i++;
				}
				else i = 0;
				
				return;
			}
	
				if(toolsBox.toggleCond[elemId] === true) {
					func[i]();
					toolsBox.toggleCond[elemId] = false;
					if(i<func.length-1) {
					i++;
					}
					else i = 0;
				}
				}
			);
			}; 

		//Вызов методов
		createForm.index();
			
	}
	
	function myOnload(elem, func, tOut) {
		if(typeof tOut === 'undefined') tOut = 0;
		elem.onload = elem.onerror = function() {
		if (!this.executed) { // выполнится только один раз
		this.executed = true;
	//Здесь выполняются скрипты
			
			func();
		}
		};
	
		elem.onreadystatechange = function() {
			var self = this;
			if (this.readyState == "complete" || this.readyState == "loaded") {
			setTimeout(function() { self.onload() }, tOut);
			}
		};
	
	}
	
	

};

function tChGoTo(url) {
		document.location.href = url;
	}